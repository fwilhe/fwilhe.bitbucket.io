#!/usr/bin/env ruby

require 'asciidoctor'

Asciidoctor.convert_file 'index.adoc', header_footer: true, safe: 'unsafe'
